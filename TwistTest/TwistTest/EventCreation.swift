//
//  EventCreation.swift
//  TwistTest
//
//  Created by Vincent Augrain on 24/04/2017.
//  Copyright © 2017 Vincent Augrain. All rights reserved.
//

import Foundation
import UIKit

//PAGE DE CREATION D'EVENT 
class EventCreationController: UIViewController, UITableViewDelegate{
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var artistTextField: UITextField!
    @IBOutlet weak var jourTextField: UITextField!
    @IBOutlet weak var moisTextField: UITextField!
    @IBOutlet weak var annéeTextField: UITextField!
    @IBOutlet weak var prixTextField: UITextField!
    @IBOutlet weak var lieuTextField: UITextField!
    @IBOutlet weak var linkTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var capacitéLabel: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    public var token = ""
    public var apiObject = apiRequest()
    
    
    
    @IBAction func submitClick(_ sender: Any) { // Fonction appelée sur appui du bouton "création" et crée un event sur api twist
        if Reachability.isConnectedToNetwork() == true{
        let mapDict = [ "name":titleTextField.text, "capacity":capacitéLabel.text]
        
        //let json = [ "title":"ABC" , "dict": mapDict ] as [String : Any]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: mapDict, options: .prettyPrinted)
            let endpoint: String = "http://163.5.84.187:5000/api/orga/events/"
            let session = URLSession.shared
            let url = NSURL(string: endpoint)!
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "POST"
            request.setValue("Token \(self.token)", forHTTPHeaderField: "Authorization")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData
            
            
            let task = session.dataTask(with: request as URLRequest){ data,response,error in
                if error != nil{
                    print(error?.localizedDescription)
                    return
                }
            }
            task.resume()
        } catch {
            print("bad things happened")
        }
        super.performSegue(withIdentifier: "CreatedEventSegue", sender: self)
        }
        else{
            alert(message: "No internet conncetion available. \n Please try again later", title: "Error")
        }
    }
    
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FirstViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) { // Préparation avant changement de vue
        if (segue.identifier == "ReturnEventCreationSegue" || segue.identifier == "CreatedEventSegue"){
            let PageVC = segue.destination as! UserInfosViewController
            PageVC.apiObject = self.apiObject
        }
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
}
