//
//  ViewController.swift
//  TwistTest
//
//  Created by Vincent Augrain on 23/10/2016.
//  Copyright © 2016 Vincent Augrain. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
// PAGE D'ACCCUEIL AVEC TOUS LES EVENTS
    @IBOutlet weak public var TestTableView: UITableView!
    @IBOutlet weak var searchBarTest: UISearchBar!
    @IBOutlet weak var SeeButton: UIButton!
    @IBOutlet weak var profil: UIButton!
    @IBOutlet weak var logoutButton: UIBarButtonItem!

    public var token = ""
    public var id = -1
    public var password = ""

    var apiObject = apiRequest()
    var retBool = false //Variable qui dit si c'est un retour sur la page d'accueil (pour éviter de recharger toutes les infos) ou si c'est la première fois qu'on y arrive
    var retApi = apiRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (!retBool) // Si ce n'est pas un retour vvvvvv
        {
            apiObject.searchApiCall(term: "") // On charge les events dans apiObject
            apiObject.addToken(totoken: token) // On ajoute le token d'identification à apiObject
            apiObject.accountId = self.id // On ajoute aussi l'ID
        }
        else{apiObject = retApi} // Si c'est un retour on prend l'apiObject de la page précedente et on utilise celle là pour pas tout recharger
        apiObject.testApiCall() // On conulte tous les events twist dans les logs
        TestTableView.reloadData() // On recharge la liste d'event à l'écran
        searchBarTest.delegate = self
        TestTableView.delegate = self

    }
    
    @IBAction func onLogoutClick(_ sender: Any) { // Fonction appelée quand on clique sur déconnexion
            if Reachability.isConnectedToNetwork() == true{ // On regarde si on a internet
                apiObject.logout() // on se deco
                apiObject.token = "no token" // on delete le token d'apiObject
                print("TOKEN DELETED")
                performSegue(withIdentifier: "logoutSegue", sender: self) // on reviens sur l'écran de connexion
            }
            else{ // Si on n'a pas internet
                alert(message: "No internet conncetion available. \n Please try again", title: "Error") // On affiche un popup qui previens qu'il a pas de connexion
            }
    }

    
    func alert(message: String, title: String = "") { //Fonction pour afficher un popup
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return apiObject.tabSize
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) { // Prépare le transfert d'informations avant le changement de vue
        if (segue.identifier == "EventPageSegue"){ // Si on va sur page de descriptif d'event
        let eventPageVC: EventPageViewController = segue.destination as! EventPageViewController
            if self.TestTableView.indexPathForSelectedRow != nil { // On transfert tout ça vvvv
                let cpt = (self.TestTableView.indexPathForSelectedRow?.row)!
                eventPageVC.tempRow = cpt
                eventPageVC.infos = apiObject.metatabtab[cpt] as! [String:AnyObject]
                eventPageVC.tempImage = apiObject.imgTab[cpt]
                eventPageVC.tmpApi = apiObject
            }
        }
        if (segue.identifier == "UserInfoSegue"){ // Si on va sur la page d'infos sur l'utilisateur
        let userInfoPage = segue.destination as! UserInfosViewController // on transfert ça vvv
        userInfoPage.apiObject = self.apiObject
        }
        
    }
    
    @IBAction func onSeeButtonClick(_ sender: UIButton) { // Fonction appelée sur le bouton de recherche
        if (sender == SeeButton){
            if Reachability.isConnectedToNetwork() == true{
                //print("searching for \(String(describing: searchBarTest.text))")
                view.endEditing(true)
                apiObject.searchApiCall(term: searchBarTest.text!)
                while (apiObject.imgTabSize < 10){
                    print("downloadding data")
                }
                TestTableView.reloadData()
            }
            else{
                alert(message: "No internet conncetion available. \n Please try again", title: "Error")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { // configuration de la liste d'event 
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellEx") as! CustomCellTableViewCell
        //tesst
        let ratio = apiObject.imgTab[indexPath.row].size.height / apiObject.imgTab[indexPath.row].size.width
        let oldViewWidth = cell.EventImage.frame.size.height
        
        cell.EventImage.image = apiObject.imgTab[indexPath.row]
        if (UIScreen.main.bounds.width * ratio - oldViewWidth > 0)
        {
            cell.EventImage.frame.size.height = UIScreen.main.bounds.width * ratio
            tableView.rowHeight = cell.EventImage.frame.size.height + 50
            
        }
        else
        {
            //cell.EventImage.frame.size.height = UIScreen.main.bounds.width * ratio
            tableView.rowHeight = cell.EventImage.frame.size.height + 50
        }
        
        
        if (cell.EventImage.frame.size.height - oldViewWidth > 0){
            cell.ImageTopConstraint.constant = (abs(cell.EventImage.frame.size.height - oldViewWidth) / 2)
            cell.EventImage.contentMode = UIViewContentMode.scaleAspectFill
        }
        else{
            
            cell.ImageTopConstraint.constant = 0
            cell.EventImage.contentMode = UIViewContentMode.scaleAspectFill
        }

        //tesst
    
        
        cell.EventDescription.text = apiObject.metatabtab[indexPath.row]["space_time_info"] as! String?
        cell.EventName.text = apiObject.metatabtab[indexPath.row]["title"] as! String?
    
        return cell
    }
    

}
