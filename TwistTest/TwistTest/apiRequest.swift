//
//  apiRequest.swift
//  TwistTest
//
//  Created by Vincent Augrain on 09/12/2016.
//  Copyright © 2016 Vincent Augrain. All rights reserved.
//

import Foundation
import UIKit

class apiRequest {

public var tabSize : Int = 0 // Taille du tableau
public var dataSetTab : [AnyObject] = [] // Tableau correspondant au dataset de la requete
public var metaTab : [String: AnyObject] = [:] // Tableau correspondant au "meta" de la requete
public var metatabtab : [AnyObject] = [] // // Tableau correspondant au tableau de meta(au dessus) de la requete
public let scriptUrl = "https://opendata.paris.fr/api/records/1.0/search/" // URL de requete
public var imgTab : [UIImage] = [] // tableau des images récupérée
public var imgTabSize = 0 // Taille du tableau d'images
public var LoadingImage = UIImage(named: "loading") // Image par défaut si aucune image disponible
public var token = "" // Token de la session chargée à l'authentification
public var accountId = -1 // ID de l'utilisateur chargée à l'authentification
    
    
    func reinitData(){ // Fonction qui clean tous les tableaux de data de la requete
        self.tabSize = 0
        self.dataSetTab.removeAll()
        self.metatabtab.removeAll()
        self.metaTab.removeAll()
        self.imgTab.removeAll()
        self.imgTabSize = 0
    }
    
    func addToken(totoken :String){ // fonction qui ajoute le token en parametre à l'objet apiRequest
        self.token = totoken
    }
    
    func loadImages(){ // Fonction qui charge toutes les images dans imgTab de apiRequest
        var cpt = 0
        var imageUrl:URL
        repeat {
            cpt = 0
            self.imgTab.removeAll()
            for _ in metatabtab{
                let tmp = self.metatabtab[cpt]["image"]!
                //print("# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #")
                if let ptr = tmp { imageUrl = URL(string: ptr as! String)!; print(tmp as Any) }
                else{ print("tried to download \(String(describing: tmp)) but nil catched");imageUrl = URL(string: "https://upload.wikimedia.org/wikipedia/commons/7/75/No_image_available.png")! }
                let imageData:NSData = NSData(contentsOf: imageUrl)!
                let image = UIImage(data: imageData as Data)
                self.imgTab.append(image!)
                self.imgTabSize = self.imgTab.count
                //print("imgTab.size = \(self.imgTabSize)")
                cpt = cpt + 1
            }
            print("1st downloadding data \(imgTabSize) and \(tabSize)")
        }while (self.imgTabSize < self.tabSize)
    }
    
func testApiCall(){ // (Il faut la renommer) Fonction qui consulte tous les events de l'api twist (affichée dans les logs)
    let urlWithParams = "http://163.5.84.187:5000/api/orga/events"
    let myUrl = NSURL(string: urlWithParams);
    let request = NSMutableURLRequest(url:myUrl! as URL);
    request.httpMethod = "GET"
    request.setValue("Token \(self.token)", forHTTPHeaderField: "Authorization")
    let task = URLSession.shared.dataTask(with: request as URLRequest) {
        data, response, error in
        if error != nil { print("error=\(String(describing: error))") ;return } // Vérification cas d'erreur
        do {                                                // Conversion JSON en dictionnaire lisible
            if let convertedJsonIntoDict: NSDictionary = try JSONSerialization.jsonObject(with: data!,options: []) as? NSDictionary {
                print("# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #")
                print("Token :" + self.token)
                print(convertedJsonIntoDict)                                                //L'affichage se fait ici
                print("# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #")
                }
        }
        catch let error{
            print(error.localizedDescription)
        }
    }
    task.resume()
}
    
func logout(){ // fonction de déconnexion qui détruit le token sur le serveur
        let urlWithParams = "http://163.5.84.187:5000/api/orga/sign-out.json"
        let myUrl = NSURL(string: urlWithParams);
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "DELETE"
        request.setValue("Token \(self.token)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if error != nil { print("error=\(String(describing: error))") ;return } // Vérification cas d'erreur
            do {                                                // Conversion JSON en dictionnaire lisible
                if let convertedJsonIntoDict: NSDictionary = try JSONSerialization.jsonObject(with: data!,options: []) as? NSDictionary {
                    print("# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #")
                    print("Token :" + self.token)
                    print(convertedJsonIntoDict)
                    print("# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #")
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
        task.resume()
}

    
    
func searchApiCall(term: String){ // Fonction qui effectue une recherche avec l'élément en parametre (sur l'api opendata paris)
    
    var urlWithParams = scriptUrl + "?dataset=evenements-a-paris"
    if(!term.isEmpty){ print("Correctly searching with term : \(term)");urlWithParams += "&q=\(term)" }
    urlWithParams = urlWithParams.replacingOccurrences(of: " ", with: "+")
    let myUrl = NSURL(string: urlWithParams);
    let request = NSMutableURLRequest(url:myUrl! as URL);
    self.reinitData()
    request.httpMethod = "GET"
    request.setValue("Token \(self.token)", forHTTPHeaderField: "Authorization")
    let task = URLSession.shared.dataTask(with: request as URLRequest) {
        data, response, error in
        if error != nil { print("error=\(String(describing: error))") ;return } // Vérification cas d'erreur
        do {                                                // Conversion JSON en dictionnaire lisible
            if let convertedJsonIntoDict: NSDictionary = try JSONSerialization.jsonObject(with: data!,options: []) as? NSDictionary {
                self.dataSetTab = (convertedJsonIntoDict["records"] as? [AnyObject])!
                self.tabSize = self.dataSetTab.count
                for object in self.dataSetTab{
                    var inter = object as? [String:Any]
                    self.metaTab = (inter?["fields"] as? [String:AnyObject])!
                    self.metatabtab.append(self.metaTab as AnyObject)
                }
            }
        }
        catch let error{
            print(error.localizedDescription)
        }
    }
    task.resume()
    while (self.tabSize < 10 || self.metatabtab.count < self.tabSize){
       print(" ", terminator: "")
    }
    self.loadImages()
}
    
}
