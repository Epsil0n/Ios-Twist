//
//  UserInfos.swift
//  TwistTest
//
//  Created by Vincent Augrain on 21/04/2017.
//  Copyright © 2017 Vincent Augrain. All rights reserved.
//

import Foundation
import UIKit


class UserInfosViewController: UIViewController, UITableViewDelegate{
    
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var NameLabelModif: UIButton!
    
    @IBOutlet weak var FirstNameLabel: UILabel!
    @IBOutlet weak var FirstNameLabelModif: UIButton!
    
    @IBOutlet weak var AddressLabel: UILabel!
    @IBOutlet weak var AddressLabelModif: UIButton!
    
    @IBOutlet weak var MailLabel: UILabel!
    @IBOutlet weak var MailLabelModif: UIButton!
    
    @IBOutlet weak var registeredLabel: UILabel!
    @IBOutlet weak var RegisteredLabelModif: UIButton!
    
    @IBOutlet weak var PasswordLabel: UILabel!
    @IBOutlet weak var PasswordLabelModif: UIButton!
    
    @IBOutlet weak var RegisterDate: UILabel!
    @IBOutlet weak var LastModifDate: UILabel!
    
    @IBOutlet weak var BackButton: UIBarButtonItem!
    @IBOutlet weak var tokenLabel: UILabel!
    
    public var passwordModif = ""
    public var passwordConfirmation = ""
    public var currentPassword = ""
    public var mailConfirmation = ""
    public var passwordChanged = 0
    
    public var apiObject = apiRequest()
    
    override func viewDidLoad() {
        getUserInfo()
        tokenLabel.text = apiObject.token
        self.passwordChanged = 0
    }
    
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func getUserInfo(){
        let urlWithParams = "http://163.5.84.187:5000/api/orga/users/" + String(apiObject.accountId)
        let myUrl = NSURL(string: urlWithParams);
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "GET"
        request.setValue("Token \(apiObject.token)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if error != nil { print("error=\(String(describing: error))") ;return } // Vérification cas d'erreur
            do {                                                // Conversion JSON en dictionnaire lisible
                if let convertedJsonIntoDict: NSDictionary = try JSONSerialization.jsonObject(with: data!,options: []) as? NSDictionary {
                    print("# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #")
                    print(convertedJsonIntoDict)
                    print("# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #")
                    let inter  = (convertedJsonIntoDict["user"] as? [String:AnyObject])!
                    
                    
                    self.RegisterDate.text = (inter["updated_at"] as? String)!
                    self.LastModifDate.text = (inter["updated_at"] as? String)!
                    self.MailLabel.text = (inter["email"] as? String)!
                }
            }
            catch let error{
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
    @IBAction func ModifInfo(_ sender: UIButton) { // Modification des informations
        let alert = UIAlertController(title: "Some Title", message: "Enter a text", preferredStyle: .alert)
        
        switch (sender)
        {
        case NameLabelModif: // Nom
            alert.setValue("Changer son nom", forKey: "title")
            alert.setValue("Veuillez entrer votre nouveau nom", forKey: "message")
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                let textField = alert?.textFields![0]
                self.NameLabel.text = textField?.text
            }))
        case FirstNameLabelModif: // Prenom
            alert.setValue("Changer son prénom", forKey: "title")
            alert.setValue("Veuillez entrer votre nouveau prénom", forKey: "message")
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                let textField = alert?.textFields![0]
                self.FirstNameLabel.text = textField?.text
            }))
        case AddressLabelModif: // Adresse
            alert.setValue("Changer son adresse", forKey: "title")
            alert.setValue("Veuillez entrer votre nouvelle adresse", forKey: "message")
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                let textField = alert?.textFields![0]
                self.AddressLabel.text = textField?.text
            }))
        case MailLabelModif: // Email
            let alert6 = UIAlertController(title: "Changer son Email", message: "Veuillez entrer votre ANCIEN mot de passe", preferredStyle: .alert)
            alert6.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                let textField = alert6.textFields![0]
                self.currentPassword = (textField.text)!
                alert?.setValue("Changer son adresse mail", forKey: "title")
                alert?.setValue("Veuillez entrer votre nouvelle adresse e-mail", forKey: "message")
                self.currentPassword = ""
                alert?.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                    let textField = alert?.textFields![0]
                    self.mailConfirmation = (textField?.text)!
                    if (self.updateEmail())
                    {self.alert(message: "E-mail correctement changé", title: "Succès")
                        self.MailLabel.text = textField?.text}
                    else
                    {self.alert(message: "Le changement a échoué. \n Veuillez réessayer plus tard.", title: "Erreur")}
                }))
                alert?.addTextField { (textField) in
                    textField.text = ""
                }
                self.present(alert!, animated: true, completion: nil)
            }))
            alert6.addTextField { (textField) in
                textField.text = ""
            }
            self.present(alert6, animated: true, completion: nil)
            
        case PasswordLabelModif:// Mot de passe
            let alert2 = UIAlertController(title: "Changer son mot de passe", message: "Veuillez entrer votre ANCIEN mot de passe", preferredStyle: .alert)
            alert2.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                let textField = alert2.textFields![0]
                self.currentPassword = (textField.text)!
                let alert3 = UIAlertController(title: "Changer son mot de passe", message: "Veuillez entrer votre NOUVEAU mot de passe", preferredStyle: .alert)
                alert3.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                    let textField = alert3.textFields![0]
                    self.passwordModif = (textField.text)!
                    let alert4 = UIAlertController(title: "Changer son mot de passe", message: "Veuillez confirmer votre NOUVEAU mot de passe", preferredStyle: .alert)
                    alert4.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                        let textField = alert4.textFields![0]
                        self.passwordConfirmation = (textField.text)!
                        if (self.passwordConfirmation == self.passwordModif)
                        {
                            self.updatePassword()
                            while (self.passwordChanged == 0)
                            {usleep(1000)}
                            if (self.passwordChanged == 1)
                            {self.alert(message: "Mot de passe correctement changé", title: "Succes"); self.passwordChanged = 0}
                            else
                            {self.alert(message: "Le changement a échoué. \n Veuillez réessayer plus tard.", title: "Erreur"); self.passwordChanged = 0}
                        }
                        else
                        {self.alert(message: "Les mots de passes sont différents. \n Essayez de nouveau", title: "Erreur")}
                        }))
                    alert4.addTextField { (textField) in
                        textField.text = ""
                    }
                    self.present(alert4, animated: true, completion: nil)
                }))
                alert3.addTextField { (textField) in
                    textField.text = ""
                }
                self.present(alert3, animated: true, completion: nil)
            }))
            alert2.addTextField { (textField) in
                textField.text = ""
            }
            self.present(alert2, animated: true, completion: nil)
        default:
            print("Integer out of range")
        }
        alert.addTextField { (textField) in textField.text = ""}
        if (sender != PasswordLabelModif) {self.present(alert, animated: true, completion: nil)}
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) // Préparation avant changement de vue
    {
        if (segue.identifier == "UserReturnSegue")
        {
            let PageVC = segue.destination as! ViewController
                PageVC.retApi = self.apiObject
                PageVC.retBool = true
        }
        if (segue.identifier == "CreateEventSegue")
        {
            let EventCreationVC = segue.destination as! EventCreationController
                EventCreationVC.token = self.apiObject.token
                EventCreationVC.apiObject = self.apiObject
        }
    }
    
    
    
    @IBAction func UserReturnToMainPage(_ sender: UIBarButtonItem)  // Fonction pour retourner sur la page principale
    {
        if (sender == BackButton)
        {
                super.performSegue(withIdentifier: "UserReturnSegue", sender: self)
        }
    }
    
    private func updatePassword(){ // Fonction de changement de mot de passe sur api twist
        let mapDict = ["current_password":self.currentPassword, "password":self.passwordModif, "password_confirmation":self.passwordConfirmation]
        let json = [ "user":mapDict ] as [String : Any]
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            let endpoint: String = "http://163.5.84.187:5000/users/" + String(apiObject.accountId) + "/update_password"
            print(endpoint)
            let session = URLSession.shared
            let url = NSURL(string: endpoint)!
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "PATCH"
            request.setValue("Token \(apiObject.token)", forHTTPHeaderField: "Authorization")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData
            let task = session.dataTask(with: request as URLRequest)
            { data,response,error in
                if error != nil
                {
                    print(error?.localizedDescription)
                    return
                }
                do
                {
                    if let convertedJsonIntoDict: NSDictionary = try JSONSerialization.jsonObject(with: data!,options: []) as? NSDictionary
                    {
                        print(convertedJsonIntoDict)
                        if (convertedJsonIntoDict["user"] != nil) {self.passwordChanged = 1;}
                        else{self.passwordChanged = 2}
                    }
                }
                catch let error{print(error.localizedDescription)}
            }
            task.resume()
        }
        catch {print("bad things happened")}
    }
    
    
    private func updateEmail()->Bool{ // Fonction de changement de mail sur api twist
        if Reachability.isConnectedToNetwork() == true{
            let mapDict = ["email":mailConfirmation, "password":self.currentPassword, "password_confirmation":self.currentPassword]
            let json = [ "user":mapDict ] as [String : Any]
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                let endpoint: String = "http://163.5.84.187:5000/users/" + String(apiObject.accountId)
                print(endpoint)
                let session = URLSession.shared
                let url = NSURL(string: endpoint)!
                let request = NSMutableURLRequest(url: url as URL)
                request.httpMethod = "PUT"
                request.setValue("Token \(apiObject.token)", forHTTPHeaderField: "Authorization")
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpBody = jsonData
                
                let task = session.dataTask(with: request as URLRequest){ data,response,error in
                    if error != nil{
                        print(error?.localizedDescription)
                        return                     }
                }
                task.resume()
            } catch {
                print("bad things happened")
            }
            return true
        }
        else{
            return false
        }
        
    }

    
    
}
