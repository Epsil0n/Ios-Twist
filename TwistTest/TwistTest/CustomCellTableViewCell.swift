//
//  CustomCellTableViewCell.swift
//  TwistTest
//
//  Created by Vincent Augrain on 25/10/2016.
//  Copyright © 2016 Vincent Augrain. All rights reserved.
//

import UIKit

class CustomCellTableViewCell: UITableViewCell { // Classe de Cellule custom pour liste d'event

    @IBOutlet weak var EventImage: UIImageView!
    @IBOutlet weak var EventName: UILabel!
    @IBOutlet weak var EventDescription: UILabel!
    @IBOutlet weak var ImageTopConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .white
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
