//
//  FirstView.swift
//  TwistTest
//
//  Created by Vincent Augrain on 24/10/2016.
//  Copyright © 2016 Vincent Augrain. All rights reserved.
//

import Foundation
import UIKit
// PAGE DE CONNEXION
class FirstViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var usernameField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var registerButton: UIButton!
    
    public var token: String = "no token"
    
    public var error: String = "no error"
    
    public var LoadingAnimation = UIActivityIndicatorView()
    
    public var id = -1
    

    func startLoading() { // Mini animation de chargement
        LoadingAnimation.center = self.view.center
        LoadingAnimation.hidesWhenStopped = true
        LoadingAnimation.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(LoadingAnimation)
        LoadingAnimation.startAnimating()
    }
    

    func stopLoading() { // Arret d'animation
        LoadingAnimation.stopAnimating()
    }
    
    override open var shouldAutorotate: Bool { //BLoque le la rotation d'écran
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        designButton()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FirstViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    
    }
    
    func designButton() // Design les boutons
    {
        registerButton.layer.cornerRadius = 5
        registerButton.layer.borderWidth = 1
        registerButton.layer.borderColor = UIColor.black.cgColor
        
        loginButton.layer.cornerRadius = 5
        loginButton.layer.borderWidth = 1
        loginButton.layer.borderColor = UIColor.black.cgColor
        
        usernameField.layer.cornerRadius = 5
        usernameField.layer.borderWidth = 1
        usernameField.layer.borderColor = UIColor.black.cgColor
        
        passwordField.layer.cornerRadius = 5
        passwordField.layer.borderWidth = 1
        passwordField.layer.borderColor = UIColor.black.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func dismissKeyboard() { // ferme le clavier
        view.endEditing(true)
    }
    
    private func askForToken(username :String, password :String){ // Fonction qui demande un token au serveur pour la connexion
            let urlWithParams = "http://163.5.84.187:5000/api/orga/sign-in.json?user_login[email]=" + username + "&user_login[password]=" + password
            print(urlWithParams)
            let myUrl = NSURL(string: urlWithParams);
            let request = NSMutableURLRequest(url:myUrl! as URL);
            request.httpMethod = "POST"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                if error != nil { print("error=\(String(describing: error))") ;return } // Vérification cas d'erreur
                do {                                                // Conversion JSON en dictionnaire lisible
                    if let convertedJsonIntoDict: [String:Any] = try JSONSerialization.jsonObject(with: data!,options: []) as? [String:Any] {
                        print(convertedJsonIntoDict)
                        if ((convertedJsonIntoDict["errors"]) != nil){
                            self.error = "error"
                            self.token = "no token"
                        }
                        if ((convertedJsonIntoDict["auth_token"]) != nil){
                            self.token = (convertedJsonIntoDict["auth_token"] as? String)!
                            self.error = "no error"
                            self.id = (convertedJsonIntoDict["id"] as? Int)!
                        }
                        
                    }
                    else{
                        print("ERROR COULD NOT REACH THE SERVER")
                    }
                }
                catch let error{
                    print(error.localizedDescription)
                }
        }
        task.resume()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) { // Préparation avant transition de vue
        if (segue.identifier == "loginSegue"){ //on transfert tout ça
            let PageVC: ViewController = segue.destination as! ViewController
            PageVC.token = self.token
            PageVC.id = self.id
            PageVC.password = self.passwordField.text!
                //print("fvc token :" + self.token)
            }
        }
    
    func alert(message: String, title: String = "") { // fonction pour afficher un popup
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func check(_ sender: AnyObject, forEvent event: UIEvent){
        startLoading()
        var cpt = 0
        if Reachability.isConnectedToNetwork() == true{ // On check si il a une connexion
        askForToken(username: usernameField.text!, password: passwordField.text!) // On fait la requete d'authentification
        while((self.token == "no token" && self.error == "no error") && cpt <= 5){// tant que la réponse du serveur n'est pas arrivée et que le timout de 6 sec n'est pas dépassé
            sleep(1)
            cpt += 1
        }
            if (cpt == 6) // Si timeout
            {
                stopLoading()
                alert(message: "Impossible d'atteindre le serveur", title: "Erreur")
            }
            else //Sinon
            {
                if (self.token == "no token" && self.error == "error" // Si retour négatif du serveur
                ){
                    stopLoading()
                    print("------NOT OK------")
                    self.error = "no error"
                    alert(message: "Wrong username/password. \n Please try again", title: "Error") // popup d'echec
                }
                else // Sinon
                {
                    print("------OK------")
                    super.performSegue(withIdentifier: "loginSegue", sender: self) // On change de vue
            }
            }
        }
        else{
            stopLoading()
            alert(message: "No internet connection available. \n Please try again", title: "Error")
        }
        
    }
}
