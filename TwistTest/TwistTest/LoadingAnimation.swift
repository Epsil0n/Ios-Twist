//
//  LoadingAnimation.swift
//  TwistTest
//
//  Created by Vincent Augrain on 16/05/2017.
//  Copyright © 2017 Vincent Augrain. All rights reserved.
//

import Foundation
import UIKit

class LoadingAnimation :UIViewController
{
    var LoadingAnimation: UIActivityIndicatorView = UIActivityIndicatorView()
    
    public func startLoadingAnimation(vue: UIViewController) //Demarre l'animation
    {
        LoadingAnimation.center = vue.view.center
        LoadingAnimation.hidesWhenStopped = true
        LoadingAnimation.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        view.addSubview(LoadingAnimation)
        LoadingAnimation.startAnimating()
        //UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    public func stopLoadinganimation() // Stop l'animation
    {
        //UIApplication.shared.endIgnoringInteractionEvents()
        LoadingAnimation.stopAnimating()
    }
}
