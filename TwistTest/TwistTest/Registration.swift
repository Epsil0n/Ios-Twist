//
//  Registration.swift
//  TwistTest
//
//  Created by Vincent Augrain on 24/04/2017.
//  Copyright © 2017 Vincent Augrain. All rights reserved.
//

import Foundation
import UIKit

//PAGE D'INSCRIPTION
class RegistrationController: UIViewController, UITableViewDelegate{
    
    @IBOutlet weak var AccountType: UISegmentedControl!
    @IBOutlet weak var FirstNameTextField: UITextField!
    @IBOutlet weak var LastNameTextField: UITextField!
    @IBOutlet weak var MailTextField: UITextField!
    @IBOutlet weak var UsernameTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBOutlet weak var PasswordConfirmationTextField: UITextField!
    var registerState = false
    
    @IBOutlet weak var RegisterButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FirstViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
    @IBAction func RegisterClick(_ sender: Any) { // Fonction appelée sur le click d'inscription
        if Reachability.isConnectedToNetwork() == true{
            if (PasswordTextField.text != PasswordConfirmationTextField.text){
                alert(message: "Les mots de passe entrez ne correspondent pas", title: "Erreur de formulaire")}
            else{
                register()
                super.performSegue(withIdentifier: "ReturnFromRegisterToConnexion", sender: self)
            }
        }
    }
    
    private func register(){ // Fonction de requete d'inscription sur api twist
        if Reachability.isConnectedToNetwork() == true{
            let mapDict = ["email":MailTextField.text, "password":PasswordTextField.text, "password_confirmation":PasswordConfirmationTextField.text]
            
            let json = [ "user":mapDict ] as [String : Any]
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                let endpoint: String = "http://163.5.84.187:5000/users"
                let session = URLSession.shared
                let url = NSURL(string: endpoint)!
                let request = NSMutableURLRequest(url: url as URL)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpBody = jsonData
                
                
                let task = session.dataTask(with: request as URLRequest){ data,response,error in
                    if error != nil{
                        print(error?.localizedDescription)
                        return
                    }
                }
                task.resume()
            } catch {
                print("bad things happened")
            }
            super.performSegue(withIdentifier: "ReturnFromRegisterToConnexion", sender: self)
        }
        else{
            alert(message: "No internet conncetion available. \n Please try again later", title: "Error")
        }

    }

}
