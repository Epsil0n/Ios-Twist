//
//  EventPageViewController.swift
//  TwistTest
//
//  Created by Vincent Augrain on 12/12/2016.
//  Copyright © 2016 Vincent Augrain. All rights reserved.
//

import Foundation
import UIKit
// PAGE DE DESCRIPTION D'EVENT
class EventPageViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var eventImage: UIImageView!
    
    @IBOutlet weak var eventName: UILabel!
    
    @IBOutlet weak var eventPrice: UILabel!
    
    @IBOutlet weak var eventDescription: UILabel!
    
    @IBOutlet weak var eventLink: UILabel!
    
    @IBOutlet weak var eventTimeLocation: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var ImageTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ImageBottomConstraint: NSLayoutConstraint!
    
    var tempImage: UIImage = UIImage()
    
    var tempRow: Int = -1
    
    var infos: [String:AnyObject] = [:]
    
    var tmpApi = apiRequest()
    
    override func viewDidLoad() { // Disposition et attribution des toutes les infos
        super.viewDidLoad()
        eventDescription.sizeToFit()
        eventName.text = infos["title"] as? String!

        let ratio = tempImage.size.height / tempImage.size.width
        let oldViewHeight = eventImage.frame.size.height
        
        eventImage.frame.size.height = ratio * UIScreen.main.bounds.width
        ImageBottomConstraint.constant = ImageBottomConstraint.constant + (abs(eventImage.frame.size.height - oldViewHeight) / 2)
        ImageTopConstraint.constant = ImageTopConstraint.constant + (abs(eventImage.frame.size.height - oldViewHeight) / 2)

        eventImage.image = tempImage
        eventImage.frame.size.height = tempImage.size.height
        eventImage.contentMode = UIViewContentMode.scaleAspectFill
        if (infos["pricing_info"] == nil){ eventPrice.text = "Non renseigné" }
        else { eventPrice.text = infos["pricing_info"] as? String!}
        eventDescription.text = infos["description"] as? String!
        let addText = infos["free_text"] as! String!
        if (addText != nil){
            eventDescription.text = eventDescription.text! + addText!
        }
        eventLink.text = infos["link"] as? String!
        eventTimeLocation.text = infos["space_time_info"] as? String!
        if let add1 = infos["adress"] as? String, let add2 = infos["city"] as? String{
        eventTimeLocation.text = eventTimeLocation.text! + add1 + add2
        }
    }
    
    func alert(message: String, title: String = "") { //Fonction popup
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func register(_ sender: Any) {// Fonction appelée sur click sur le bouton d'inscription à un event
        alert(message: "You are sucessfuly registered.", title: "Success")
        registerButton.isEnabled = false
        registerButton.setTitle("Inscrit", for: UIControlState.normal)
        registerButton.backgroundColor = .gray
        registerButton.setTitleColor(.white, for: .normal)

        //super.performSegue(withIdentifier: "returnSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) { // Préparation avant changement de vue
        if (segue.identifier == "returnSegue"){
            let retPage: ViewController = segue.destination as! ViewController
                retPage.retBool = true
                retPage.retApi = tmpApi
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
